this["views"] = this["views"] || {};

this["views"]["./app/js/views/admin/admin.hbs"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "        <tr class=\"book\" id=\"book-"
    + alias3(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"_id","hash":{},"data":data}) : helper)))
    + "\">\n            <td class=\"inline\" data-key=\"title\" data-type=\"Stories\" data-search=\"true\" data-id=\""
    + alias3(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"_id","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"title","hash":{},"data":data}) : helper)))
    + "</td>\n            <td class=\"inline\" data-key=\"publisher\" data-type=\"Stories\" data-id=\""
    + alias3(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"_id","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.publisher || (depth0 != null ? depth0.publisher : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"publisher","hash":{},"data":data}) : helper)))
    + "</td>\n            <td class=\"inline\" data-key=\"author\" data-type=\"Stories\" data-id=\""
    + alias3(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"_id","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.author || (depth0 != null ? depth0.author : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"author","hash":{},"data":data}) : helper)))
    + "</td>\n            <td class=\"inline\" data-key=\"illustrator\" data-type=\"Stories\" data-id=\""
    + alias3(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"_id","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.illustrator || (depth0 != null ? depth0.illustrator : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"illustrator","hash":{},"data":data}) : helper)))
    + "</td>\n            <td class=\"inline\" data-key=\"price\" data-type=\"Stories\" data-id=\""
    + alias3(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"_id","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.price || (depth0 != null ? depth0.price : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"price","hash":{},"data":data}) : helper)))
    + "</td>\n            <td class=\"inline\" data-key=\"isbn\" data-type=\"Stories\" data-id=\""
    + alias3(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"_id","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.isbn || (depth0 != null ? depth0.isbn : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"isbn","hash":{},"data":data}) : helper)))
    + "</td>\n            <td class=\"inline\" data-key=\"approx_reading_time\" data-type=\"Stories\" data-id=\""
    + alias3(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"_id","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.approx_reading_time || (depth0 != null ? depth0.approx_reading_time : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"approx_reading_time","hash":{},"data":data}) : helper)))
    + "</td>\n            <td><span style=\"cursor:pointer;\" class=\"fa fa-pencil\" data-href=\"/admin/stories/"
    + alias3(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"_id","hash":{},"data":data}) : helper)))
    + "\"></span> <span class=\"fa fa-trash\" data-id=\""
    + alias3(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"_id","hash":{},"data":data}) : helper)))
    + "\" data-type=\"Stories\" blklab-click=\"remove\"></span></td>\n        </tr>\n";
},"3":function(depth0,helpers,partials,data) {
    return "        <tr>\n            <td colspan=\"5\">No Books</td>\n        </tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<section id=\"wrapper\">\n    <h2>Books</h2>\n    <div class=\"row\">\n        <input type=\"search\" id=\"search\" blklab-typing=\"search\" data-fields=\"title,publisher,author,illustrator,isbn\" placeholder=\"Search title, publisher, author, illustrator or isbn\" style=\"width:50%;\">\n    </div>\n\n    <table border=\"1\" class=\"bordered\" id=\"data\">\n        <tr>\n            <th>Book Title</th>\n            <th>Publisher</th>\n            <th>Author</th>\n            <th>Illustrator</th>\n            <th>Price</th>\n            <th>ISBN</th>\n            <th>Reading Time</th>\n            <th></th>\n        </tr>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.data : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "    </table>\n\n    <br>\n    <br>\n    <br>\n    <br>\n    <br>\n\n    <span blklab-click=\"addBook\" style=\"font-size:24px;\">+</span>\n\n    <div id=\"book-detail\" class=\"hide\">\n        <h2>Add Book</h2>\n\n        <form blklab-submit=\"createBook\" id=\"form\">\n\n                <div class=\"row\">\n                    <label>Book Title</label>\n                    <input type=\"text\" name=\"title\">\n                </div>\n\n                <div class=\"row\">\n                    <label>Author</label>\n                    <input type=\"text\" name=\"author\">\n                </div>\n\n                <div class=\"row\">\n                    <label>Illustrator</label>\n                    <input type=\"text\" name=\"illustrator\">\n                </div>\n\n                <div class=\"row\">\n                    <label>Price</label>\n                    <input type=\"text\" name=\"price\">\n                </div>\n\n                <div class=\"row\">\n                    <label>Difficulty</label>\n                    <input type=\"text\" name=\"difficulty\">\n                </div>\n\n                <div class=\"row\">\n                    <label>ISBN</label>\n                    <input type=\"text\" name=\"isbn\">\n                </div>\n\n                <div class=\"row\">\n                    <label>Description</label>\n                    <input type=\"text\" name=\"Description\">\n                </div>\n            <div class=\"row\">\n                <input type=\"submit\" id=\"submit\" value=\"Save Book\">\n            </div>\n\n        </form>\n    </div>\n\n</section>\n\n";
},"useData":true});

this["views"]["./app/js/views/admin/dashboard.hbs"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=this.lambda, alias2=this.escapeExpression;

  return "<header>\n    <h1>StorySnap Dashboard</h1>\n</header>\n\n<div class=\"tabs\">\n    <ul>\n        <li class=\"active click\" data-href=\"/admin/dashboard\">Overview</li>\n        <li>Metrics</li>\n        <li class=\"click\" data-href=\"/admin/dashboard/filter\">Filters</li>\n    </ul>\n</div>\n\n<section>\n    <section class=\"grid\">\n        <h3>User Overview</h3>\n        <div class=\"grid-cell grid-cell-1of4\">\n            <label>Total Users</label>\n            <p class=\"total\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.totalUsers : stack1), depth0))
    + "</p>\n            <div class=\"border\"></div>\n        </div>\n\n        <div class=\"grid-cell grid-cell-1of4\">\n            <label>Total Accounts Activated</label>\n            <p class=\"total\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.totalUsersActivated : stack1), depth0))
    + "</p>\n            <p class=\"percent\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.totalUsersActivatedPercent : stack1), depth0))
    + "</p>\n            <div class=\"border\"></div>\n        </div>\n\n        <div class=\"grid-cell grid-cell-1of4\">\n            <label>Total Active Users Who Have Yet To Perform Action</label>\n            <p class=\"total\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.totalUsersActivatedNoAction : stack1), depth0))
    + "</p>\n            <p class=\"percent\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.totalUsersActivatedNoActionPercent : stack1), depth0))
    + "</p>\n        </div>\n\n        <div class=\"divider\"></div>\n\n        <h3>User Activity</h3>\n\n        <div class=\"grid-cell grid-cell-1of4\">\n            <label>Total Users Who Have Purchased a Story</label>\n            <p class=\"total\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.totalUsersPurchasedStory : stack1), depth0))
    + "</p>\n            <p class=\"percent\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.totalUsersPurchasedStoryPercent : stack1), depth0))
    + "</p>\n            <div class=\"border\"></div>\n        </div>\n\n        <div class=\"grid-cell grid-cell-1of4\">\n            <label>Total Users Who Have Recorded a Story</label>\n            <p class=\"total\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.totalUsersRecorded : stack1), depth0))
    + "</p>\n            <p class=\"percent\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.totalUsersRecordedPercent : stack1), depth0))
    + "</p>\n            <div class=\"border\"></div>\n        </div>\n\n        <div class=\"grid-cell grid-cell-1of4\">\n            <label>Total Users Who Have Shared A Story</label>\n            <p class=\"total\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.totalUsersShared : stack1), depth0))
    + "</p>\n            <p class=\"percent\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.totalUsersSharedPercent : stack1), depth0))
    + "</p>\n            <div class=\"border\"></div>\n        </div>\n\n        <div class=\"grid-cell grid-cell-1of4\">\n            <label>Total Users Who Have Invited Someone to Read a Story</label>\n            <p class=\"total\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.totalUsersInvited : stack1), depth0))
    + "</p>\n            <p class=\"percent\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.totalUsersInvitedPercent : stack1), depth0))
    + "</p>\n\n        </div>\n\n        <div class=\"divider\"></div>\n\n        <div class=\"grid-cell grid-cell-1of4\">\n            <label>Total Invites That Have Been Accepted</label>\n            <p class=\"total\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.totalUsersAcceptedInvites : stack1), depth0))
    + "</p>\n            <p class=\"percent\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.totalUsersAcceptedInvitesPercent : stack1), depth0))
    + "</p>\n            <div class=\"border\"></div>\n        </div>\n\n        <div class=\"grid-cell grid-cell-1of4\">\n            <label>Total Invites That Have Been Withdrawn</label>\n            <p class=\"total\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.totalUsersWithdrawInvites : stack1), depth0))
    + "</p>\n            <p class=\"percent\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.totalUsersWithdrawInvitesPercent : stack1), depth0))
    + "</p>\n            <div class=\"border\"></div>\n        </div>\n\n        <div class=\"divider\"></div>\n\n        <h3>User Profile</h3>\n\n        <div class=\"grid-cell grid-cell-1of4\">\n            <label>Total Users Who Have Full Profile</label>\n            <p class=\"total\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.totalUsersFullProfile : stack1), depth0))
    + "</p>\n            <p class=\"percent\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.totalUsersFullProfilePercent : stack1), depth0))
    + "</p>\n            <div class=\"border\"></div>\n        </div>\n\n\n        <div class=\"grid-cell grid-cell-1of4\">\n            <label>Total Users With Profile Image</label>\n            <p class=\"total\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.totalUsersProfileImage : stack1), depth0))
    + "</p>\n            <p class=\"percent\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.totalUsersProfileImagePercent : stack1), depth0))
    + "</p>\n            <div class=\"border\"></div>\n        </div>\n\n        <div class=\"divider\"></div>\n\n    </section>\n</section>\n";
},"useData":true});

this["views"]["./app/js/views/admin/edit_story.hbs"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
    return "                    <option value=\"false\">No</option>\n                    <option value=\"true\">Yes</option>\n";
},"3":function(depth0,helpers,partials,data) {
    return "                        <option value=\"0\">1-10</option>\n                        <option value=\"1\">11-20</option>\n                        <option value=\"2\">21-30</option>\n                        <option value=\"3\">31+</option>\n";
},"5":function(depth0,helpers,partials,data) {
    return "                        <option value=\"false\">Picture</option>\n                        <option value=\"true\">Illustrated</option>\n";
},"7":function(depth0,helpers,partials,data) {
    return "                        <option value=\"false\">One-off</option>\n                        <option value=\"true\">Series</option>\n";
},"9":function(depth0,helpers,partials,data) {
    return "                        <option value=\"false\">Prose</option>\n                        <option value=\"true\">Poetry</option>\n";
},"11":function(depth0,helpers,partials,data) {
    var helper, alias1=this.escapeExpression;

  return "                    <tr>\n                        <td style=\"text-align:left;\">Page #"
    + alias1(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "</td><td style=\"text-align:left;\">"
    + alias1(this.lambda(depth0, depth0))
    + "</td><td><span class=\"fa fa-trash\"></span></td>\n                    </tr>\n";
},"13":function(depth0,helpers,partials,data) {
    var stack1;

  return "                <table>\n"
    + ((stack1 = helpers.each.call(depth0,((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.tips : stack1),{"name":"each","hash":{},"fn":this.program(14, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </table>\n";
},"14":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression, alias4=this.lambda;

  return "                    <tr>\n                        <td style=\"text-align:left;\">Tip #"
    + alias3(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "</td><td class=\"edit\" data-type=\"tip\" data-id=\""
    + alias3(alias4(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1._id : stack1), depth0))
    + "\" data-idx=\""
    + alias3(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "\" style=\"text-align:left;\">"
    + alias3(alias4(depth0, depth0))
    + "</td><td><span class=\"fa fa-trash\"></span></td>\n                    </tr>\n";
},"16":function(depth0,helpers,partials,data) {
    var stack1;

  return "                <table>\n"
    + ((stack1 = helpers.each.call(depth0,((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.images : stack1),{"name":"each","hash":{},"fn":this.program(17, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                </table>\n";
},"17":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "                        <tr>\n                            <td style=\"text-align:left;\">Tip #"
    + alias3(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "</td><td class=\"edit\" data-type=\"tip\" data-id=\""
    + alias3(this.lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1._id : stack1), depth0))
    + "\" data-idx=\""
    + alias3(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "\" style=\"text-align:left;\">&nbsp;</td><td><span class=\"fa fa-trash\"></span></td>\n                        </tr>\n";
},"19":function(depth0,helpers,partials,data) {
    var stack1, helper, alias1=this.escapeExpression;

  return "                    <tr>\n                        <td class=\"edit\" data-type=\"tip\" data-id=\""
    + alias1(this.lambda(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1._id : stack1), depth0))
    + "\" data-idx=\""
    + alias1(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "\" style=\"text-align:left;\">&nbsp;</td><td><span class=\"fa fa-trash\"></span></td>\n                    </tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, alias1=helpers.helperMissing, alias2=this.lambda, alias3=this.escapeExpression;

  return "<section id=\"wrapper\">\n    <a href=\"/admin\">Back</a>\n\n    <form blklab-submit=\"save\" id=\"form\">\n        <h2>Edit Book</h2>\n\n        <div class=\"divider\"></div>\n\n        <div class=\"row\">\n            <label>Active</label>\n            <select name=\"active\">\n"
    + ((stack1 = (helpers.select || (depth0 && depth0.select) || alias1).call(depth0,((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.active : stack1),{"name":"select","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </select>\n        </div>\n\n        <div class=\"divider\"></div>\n\n        <section class=\"col\">\n\n            <h3>Detail Fields</h3>\n            <div class=\"row\">\n                <label>Book Title</label>\n                <input type=\"text\" name=\"title\" value=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.title : stack1), depth0))
    + "\" placeholder=\"Book Title\">\n            </div>\n\n            <div class=\"row\">\n                <label>Author</label>\n                <input type=\"text\" name=\"author\" value=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.author : stack1), depth0))
    + "\" placeholder=\"Author\">\n            </div>\n\n            <div class=\"row\">\n                <label>Author Surname</label>\n                <input type=\"text\" name=\"author_surname\" value=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.author_surname : stack1), depth0))
    + "\" placeholder=\"\">\n            </div>\n\n            <div class=\"row\">\n                <label>Author Link</label>\n                <input type=\"text\" name=\"author_link\" value=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.author_link : stack1), depth0))
    + "\" placeholder=\"\">\n            </div>\n\n            <div class=\"row\">\n                <label>Illustrator</label>\n                <input type=\"text\" name=\"illustrator\" value=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.illustrator : stack1), depth0))
    + "\" placeholder=\"\">\n            </div>\n\n            <div class=\"row\">\n                <label>Illustrator Surname</label>\n                <input type=\"text\" name=\"illustrator_surname\" value=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.illustrator_surname : stack1), depth0))
    + "\" placeholder=\"\">\n            </div>\n\n            <div class=\"row\">\n                <label>Illustrator Link</label>\n                <input type=\"text\" name=\"illustrator_link\" value=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.illustrator_link : stack1), depth0))
    + "\" placeholder=\"\">\n            </div>\n\n            <div class=\"row\">\n                <label>Publisher</label>\n                <input type=\"text\" name=\"publisher\" value=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.publisher : stack1), depth0))
    + "\" placeholder=\"Publisher\">\n            </div>\n\n            <div class=\"row\">\n                <label>ISBN</label>\n                <input type=\"text\" name=\"isbn\" value=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.isbn : stack1), depth0))
    + "\" placeholder=\"ISBN\">\n            </div>\n\n            <div class=\"row\">\n                <label>Language</label>\n                <input type=\"text\" name=\"language\" value=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.language : stack1), depth0))
    + "\" placeholder=\"English, French, etc.\">\n            </div>\n\n            <div class=\"row\">\n                <label>Age Range</label>\n                <input type=\"text\" name=\"age_range\" value=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.age_range : stack1), depth0))
    + "\" placeholder=\"\">\n            </div>\n\n            <div class=\"row\">\n                <label>Price</label>\n                <input type=\"text\" name=\"price\" value=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.price : stack1), depth0))
    + "\" placeholder=\"Price\">\n            </div>\n\n            <div class=\"row\">\n                <label>Featured Weight</label>\n                <input type=\"text\" name=\"weight\" value=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.weight : stack1), depth0))
    + "\" placeholder=\"\">\n            </div>\n\n            <div class=\"row\">\n                <label>Estimated reading time</label>\n                <select name=\"approx_reading_time\">\n"
    + ((stack1 = (helpers.select || (depth0 && depth0.select) || alias1).call(depth0,((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.approx_reading_time : stack1),{"name":"select","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                </select>\n            </div>\n\n            <div class=\"row\">\n                <label>Synopsis</label>\n                <textarea name=\"description\" placeholder=\"Synopsis\" style=\"width:75%; height:400px;\">"
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.description : stack1), depth0))
    + "</textarea>\n            </div>\n\n            <div class=\"row\">\n                <label>Preview Pages</label>\n                <input type=\"text\" name=\"preview_pages\" value=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.preview_pages : stack1), depth0))
    + "\" placeholder=\"Ex. 1,2,3\">\n            </div>\n\n            <div class=\"row\">\n                <label>Apple Product Identifier</label>\n                <input type=\"text\" name=\"apple_id\" value=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.apple_id : stack1), depth0))
    + "\" placeholder=\"\">\n            </div>\n        </section>\n\n        <section class=\"col\">\n            <h3>Sorting/Filtering Fields</h3>\n\n            <div class=\"row\">\n                <label>Publication Date</label>\n                <input type=\"text\" name=\"publication_date\" value=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.publication_date : stack1), depth0))
    + "\" placeholder=\"\">\n            </div>\n\n            <div class=\"row\">\n                <label>Copyright</label>\n                <input type=\"text\" name=\"copyright\" value=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.copyright : stack1), depth0))
    + "\" placeholder=\"\">\n            </div>\n\n            <div class=\"row\">\n                <label>Pages</label>\n                <input type=\"text\" name=\"pages\" value=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.pages : stack1), depth0))
    + "\" placeholder=\"\">\n            </div>\n\n            <div class=\"row\">\n                <label>Date Added to StorySnap</label>\n                <input type=\"text\" name=\"pages\" value=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.date_added : stack1), depth0))
    + "\" placeholder=\"\">\n            </div>\n\n            <div class=\"row\">\n                <label>Difficulty</label>\n                <input type=\"text\" name=\"difficulty\" value=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.difficulty : stack1), depth0))
    + "\" placeholder=\"Difficulty\">\n            </div>\n\n            <div class=\"row\">\n                <label>Picture Book or Illustrated Chapter Book</label>\n                <select name=\"illustrated\">\n"
    + ((stack1 = (helpers.select || (depth0 && depth0.select) || alias1).call(depth0,((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.illustrated : stack1),{"name":"select","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                </select>\n            </div>\n\n            <div class=\"row\">\n                <label>One-off book or part of a series</label>\n                <select name=\"series\">\n"
    + ((stack1 = (helpers.select || (depth0 && depth0.select) || alias1).call(depth0,((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.series : stack1),{"name":"select","hash":{},"fn":this.program(7, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                </select>\n            </div>\n\n            <div class=\"row\">\n                <label>Poetry or Prose</label>\n                <select name=\"poetry\">\n"
    + ((stack1 = (helpers.select || (depth0 && depth0.select) || alias1).call(depth0,((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.poetry : stack1),{"name":"select","hash":{},"fn":this.program(9, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "                </select>\n            </div>\n        </section>\n\n        <div class=\"clear\"></div>\n\n        <div class=\"row\">\n            <input type=\"hidden\" name=\"_id\" value=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1._id : stack1), depth0))
    + "\">\n            <input type=\"submit\" id=\"submit\" value=\"Save\"> <input type=\"button\" value=\"Cancel\" data-href=\"/admin\">\n        </div>\n\n\n        <div class=\"divider\"></div>\n\n        <!--<div class=\"row\">\n            <label>Pages</label>\n            <table>\n"
    + ((stack1 = helpers.each.call(depth0,((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.images : stack1),{"name":"each","hash":{},"fn":this.program(11, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </table>\n        </div>-->\n\n        <div class=\"row\">\n            <label>Tips</label>\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.tips : stack1),{"name":"if","hash":{},"fn":this.program(13, data, 0),"inverse":this.program(16, data, 0),"data":data})) != null ? stack1 : "")
    + "        </div>\n\n        <div class=\"row\">\n            <label>Objective Categories</label>\n            <table id=\"obj-cats\">\n"
    + ((stack1 = helpers.each.call(depth0,((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.objective_categories : stack1),{"name":"each","hash":{},"fn":this.program(19, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </table>\n            <input id=\"ob-cat-input\"> <span blklab-click=\"addObCat\">+</span>\n        </div>\n\n        <div class=\"row\">\n            <label>Subjective Categories</label>\n            <table id=\"sub-cats\">\n"
    + ((stack1 = helpers.each.call(depth0,((stack1 = (depth0 != null ? depth0.data : depth0)) != null ? stack1.subjective_categories : stack1),{"name":"each","hash":{},"fn":this.program(19, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </table>\n            <input id=\"sub-cat-input\"> <span blklab-click=\"addSubCat\">+</span>\n        </div>\n\n\n\n    </form>\n\n    <br>\n    <br>\n    <br>\n    <br>\n    <br>\n\n    <h2>Load Files</h2>\n\n    <div class=\"row\">\n        <label>Has Cropmarks?</label>\n        <select id=\"cropmarks\">\n            <option value=\"0\">NO</option>\n            <option value=\"1\">YES</option>\n        </select>\n    </div>\n\n    <div class=\"row\">\n        <label>Assets</label>\n        <input type=\"file\" blklab-change=\"loadFiles\">\n    </div>\n\n    <div class=\"row\">\n        <input type=\"button\" blklab-click=\"optimize\" value=\"Optimize\">\n    </div>\n\n    <div class=\"row\">\n        <input type=\"button\" blklab-click=\"genThumb\" value=\"Generate Thumbnail\">\n    </div>\n\n    <div class=\"row\">\n        <input type=\"button\" blklab-click=\"genFeatured\" value=\"Generate Featured Image\">\n    </div>\n\n</section>\n";
},"useData":true});

this["views"]["./app/js/views/admin/filters.hbs"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "                    <tr>\n                        <td style=\"padding:20px 10px; text-transform: capitalize;\">"
    + alias3(((helper = (helper = helpers.first_name || (depth0 != null ? depth0.first_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"first_name","hash":{},"data":data}) : helper)))
    + "</td>\n                        <td style=\"padding:20px 10px; text-transform: capitalize;\">"
    + alias3(((helper = (helper = helpers.last_name || (depth0 != null ? depth0.last_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"last_name","hash":{},"data":data}) : helper)))
    + "</td>\n                        <td style=\"padding:20px 10px;\">"
    + alias3(((helper = (helper = helpers.email || (depth0 != null ? depth0.email : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"email","hash":{},"data":data}) : helper)))
    + "</td>\n                    </tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<header>\n    <h1>StorySnap Dashboard</h1>\n</header>\n\n<div class=\"tabs\">\n    <ul>\n        <li class=\"click\" data-href=\"/admin/dashboard\">Overview</li>\n        <li>Metrics</li>\n        <li class=\"active click\" data-href=\"/admin/dashboard/filter\">Filters</li>\n    </ul>\n</div>\n\n<section>\n    <section class=\"grid\">\n        <h3><span id=\"filteredBy\">Filters</span>\n            <select id=\"filterBy\" blklab-change=\"filterBy\" style=\"float:right; margin-right:20px 10px;\">\n                <option value=\"profiles\">All Users</option>\n                <option value=\"no-action\">Created profile but not taken any other action</option>\n                <option value=\"purchase-not-recorded\">Purchased a story but not recorded it</option>\n                <option value=\"recorded-not-shared\">Purchased and recorded a story but not shared it</option>\n                <option value=\"recorded-and-shared\">Purchased, recorded and shared a story</option>\n                <option value=\"invite-sent\">Invited someone to record a story</option>\n                <option value=\"invite-accepted\">Accepted an invitation to record a story</option>\n                <option value=\"invite-accepted-and-recorded\">Accepted invitation and recorded a story</option>\n            </select>\n        </h3>\n\n        <div class=\"grid-cell\">\n            <table>\n                <tr>\n                    <th style=\"padding:20px 10px; text-transform: capitalize;\">First Name</th>\n                    <th style=\"padding:20px 10px; text-transform: capitalize;\">Last Name</th>\n                    <th style=\"padding:20px 10px;\">Email</th>\n                </tr>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.data : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </table>\n\n        </div>\n\n        <div class=\"divider\"></div>\n\n    </section>\n</section>\n";
},"useData":true});

this["views"]["./app/js/views/admin/users.hbs"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "        <tr class=\"book\" id=\"book-"
    + alias3(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"_id","hash":{},"data":data}) : helper)))
    + "\">\n            <td class=\"inline\" data-key=\"first_name\" data-type=\"Users\" data-search=\"true\" data-id=\""
    + alias3(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"_id","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.first_name || (depth0 != null ? depth0.first_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"first_name","hash":{},"data":data}) : helper)))
    + "</td>\n            <td class=\"inline\" data-key=\"last_name\" data-type=\"Users\" data-search=\"true\" data-id=\""
    + alias3(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"_id","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.last_name || (depth0 != null ? depth0.last_name : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"last_name","hash":{},"data":data}) : helper)))
    + "</td>\n            <td class=\"inline\" data-key=\"email\" data-type=\"Users\" data-search=\"true\" data-id=\""
    + alias3(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"_id","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.email || (depth0 != null ? depth0.email : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"email","hash":{},"data":data}) : helper)))
    + "</td>\n            <td class=\"inline\" data-key=\"confirmed\" data-type=\"Users\" data-id=\""
    + alias3(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"_id","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3(((helper = (helper = helpers.confirmed || (depth0 != null ? depth0.confirmed : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"confirmed","hash":{},"data":data}) : helper)))
    + "</td>\n            <td><span style=\"cursor:pointer;\" class=\"fa fa-pencil\" data-href=\"/admin/users/"
    + alias3(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"_id","hash":{},"data":data}) : helper)))
    + "\"></span> <span class=\"fa fa-trash\" data-id=\""
    + alias3(((helper = (helper = helpers._id || (depth0 != null ? depth0._id : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"_id","hash":{},"data":data}) : helper)))
    + "\" data-type=\"Users\" blklab-click=\"remove\"></span></td>\n        </tr>\n";
},"3":function(depth0,helpers,partials,data) {
    return "        <tr>\n            <td colspan=\"5\">No Users</td>\n        </tr>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<section id=\"wrapper\">\n    <h2>Users</h2>\n    <div class=\"row\">\n        <input type=\"search\" id=\"search\" blklab-typing=\"search\" data-fields=\"first_name,last_name,email\" placeholder=\"Search First Name, Last Name, Email\" style=\"width:50%;\">\n    </div>\n\n    <table border=\"1\" class=\"bordered\" id=\"data\">\n        <tr>\n            <th>First Name</th>\n            <th>Last Name</th>\n            <th>Email</th>\n            <th>Confirmed</th>\n            <th></th>\n        </tr>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.data : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "    </table>\n\n</section>\n\n";
},"useData":true});

this["views"]["./app/js/views/users/recover.hbs"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<section id=\"home\">\n    <section id=\"hero\" class=\"professor fade\">\n        <div class=\"overlay\">\n            <header class=\"inner\">\n                <h1>StorySnap</h1>\n\n            </header>\n\n\n            <section id=\"forgot\">\n                <form class=\"onboarding\" id=\"onboarding\" blklab-submit=\"login\">\n                    <h2>Password Reset</h2>\n                    <div class=\"row\">\n                        <input type=\"password\" name=\"password\" id=\"password_field\" placeholder=\"Password\" class=\"icon lock\">\n                    </div>\n                    <div id=\"reset-form-message\" class=\"center alert\"></div>\n                    <input type=\"button\" value=\"Update Password\" class=\"inline\" blklab-click=\"updatePassword\" id=\"forgotButton\">\n                </form>\n            </section>\n\n        </div>\n    </section>\n</section>\n";
},"useData":true});;BlkLab.App.UsersModel = BlkLab.Model.extend({});
BlkLab.App.UsersModel.url = '/api/users';

BlkLab.App.UsersResetView = BlkLab.View.extend({
    template: this.views['./app/js/views/users/recover.hbs'],
    title:'Kubby Professor Home'
});

BlkLab.App.UsersResetController = BlkLab.Controller.extend({
    token: '',

    actions:{
        updatePassword: function(){
            var self = BlkLab.App.UsersResetController;
            var password = _$('#password_field').value;
            var t = location.pathname.split('/').pop()
            BlkLab.post({
                url: '/api/users/auth/reset/' + self.token,
                data: JSON.stringify({
                    password: password
                }),
            }).then(function(http){
                var resp = JSON.parse(http.responseText);
                if(resp.token){
                    alert("Your password has been reset").
                    window.close();
                }else{
                    _$('#reset-form-message').innerHTML = 'Error Resetting Your Password<br><br>';
                }
            })
        }
    },

    render: function(params){
        this.token = params.id;
        var view = new BlkLab.App.UsersResetView();
        view.model = {
            data:{}
        };
        view.render('#content');
        this.refreshActions();
    }
});

BlkLab.App.Router.routes({
    '/users/reset/:id': {
        controller: BlkLab.App.UsersResetController
    }
});
;BlkLab.App.StoriesModel = BlkLab.Model.extend({});
BlkLab.App.StoriesModel.url = '/api/stories';

BlkLab.App.StatsModel = BlkLab.Model.extend({});
BlkLab.App.StatsModel.url = '/api/stats';

BlkLab.App.FilterModel = BlkLab.Model.extend({
    first_name: String,
    last_name: String,
    email: String
});
BlkLab.App.FilterModel.url = '/api/stats/filter';

BlkLab.App.AdminView = BlkLab.View.extend({
    template: this.views['./app/js/views/admin/admin.hbs']
});

BlkLab.App.AdminEditStoryView = BlkLab.View.extend({
    template: this.views['./app/js/views/admin/edit_story.hbs']
});

BlkLab.App.AdminUsersView = BlkLab.View.extend({
    template: this.views['./app/js/views/admin/users.hbs']
});

BlkLab.App.AdminDashboardView = BlkLab.View.extend({
    template: this.views['./app/js/views/admin/dashboard.hbs']
});

BlkLab.App.AdminFiltersView = BlkLab.View.extend({
    template: this.views['./app/js/views/admin/filters.hbs']
});

BlkLab.App.AdminController = BlkLab.Controller.extend({
    _id: '',

    actions:{
        createBook: function(ev){
            ev.preventDefault();
            var model = Object.create(BlkLab.App.StoriesModel);
            model.setData(BlkLab.Form.collect('form'));
            model.save().then(function(){
                location.reload(true);
            });
            return false;
        },

        loadFiles: function(ev){
            var cropmarks = _$('#cropmarks').value;
            for (var i = 0; i < this.files.length; i++) {
                BlkLab.Uploader.add_to_queue(this.files[i], {cropmarks: cropmarks, url: BlkLab.imageProcessor + '/process-assets/' + BlkLab.App.AdminController._id}, function(e){});
            }
            BlkLab.Uploader.onFinish = function(){
                location.reload(0);
            }
            BlkLab.Uploader.start();
        },

        search: function(){
            var self = BlkLab.App.AdminController;
            var ele = _$(this);
            var fields = ele.data('fields').split(',');

            var tags = _$('.book');
            var tag;
            var key;
            var val = ele.value.toLowerCase();

            self.data.forEach(function(row){
                var found = false;
                var tag = _$('#book-' + row._id);

                fields.forEach(function(key){
                    if(row[key]){
                        var value = row[key].toLowerCase();
                        if(found){
                            found = true;
                        }else if(!found && (val == "" || value.indexOf(val) != -1)){
                            found = true;
                        }else{
                            found = false;
                        }
                    }
                });

                if(found){
                    tag.css({
                        display:'table-row'
                    });
                }else{
                    tag.hide();
                }
            });
        },

        addBook: function(){
            if(_$('#book-detail').has_class('hide')){
                this.innerHTML = 'X';
            }else{
                this.innerHTML = '+';
            }

            _$('#book-detail').toggle_class('hide');
        }
    },
    data: [],

    inline: function(){
        var self = BlkLab.App.AdminController;
        var cells = _$('.inline');
        cells.click(function(ev){
            if(ev.target.nodeName != 'INPUT'){
                var cell = _$(this);
                var input = BlkLab.create('input');
                var current = this.innerHTML;
                input.value = this.innerHTML;
                cell.innerHTML = '';
                cell.append(input);
                input.focus();
                input.onblur(function(){
                    if(current != input.value){
                        var model = Object.create(BlkLab.App[cell.data('type') +'Model']);
                        model.find({'_id': cell.data('id')}).then(function(m){
                            model.set(cell.data('key'), input.value);
                            model.save().then(function(http){
                                console.log(http.responseText);
                            });
                        });
                    }
                    cell.innerHTML = input.value;
                });
            }
        });
    },

    render: function(params){
        var self = this;
        var view = new BlkLab.App.AdminView();
        var model = Object.create(BlkLab.App.StoriesModel);
        model.url = '/api/stories/all';
        model.find().then(function(model){
            self.data = model.data;
            view.model = model;
            view.render('#content');
            self.inline();
            self.refreshActions();

            _$('#search').on('change', self.actions.search);
        });
    }
});

BlkLab.App.AdminEditStoryController = BlkLab.Controller.extend({
    _id: '',
    tips: [],
    images: [],
    objective_categories: [],
    subjective_categories: [],

    actions:{
        save: function(ev){
            ev.preventDefault();
            var model = Object.create(BlkLab.App.StoriesModel);
            var formdata = BlkLab.Form.collect('form');
            model.setData(formdata);
            model.save({'_id': formdata._id}).then(function(){
                //location.reload(true);
            });
            return false;
        },

        optimize: function(){
            BlkLab.post({
                url: '/api/stories/optimize/' + BlkLab.App.AdminEditStoryController._id
            }).then(function(http){
                alert('done');
            });
        },

        genThumb: function(){
            BlkLab.post({
                url: '/api/stories/create/thumbnails/' + BlkLab.App.AdminEditStoryController._id
            }).then(function(http){
                alert('done');
            });
        },

        genFeatured: function(){
            BlkLab.post({
                url: '/api/stories/create/featured/' + BlkLab.App.AdminEditStoryController._id
            }).then(function(http){
                alert('done');
            });
        },

        loadFiles: function(ev){
            var cropmarks = _$('#cropmarks').value;
            for (var i = 0; i < this.files.length; i++) {
                BlkLab.Uploader.add_to_queue(this.files[i], {cropmarks: cropmarks, url: BlkLab.imageProcessor + '/process-assets/' + BlkLab.App.AdminEditStoryController._id}, function(e){});
            }
            BlkLab.Uploader.onFinish = function(){
                _$('#book-detail').show();
            }
            BlkLab.Uploader.start();
        },

        addObCat: function(){
            var self = BlkLab.App.AdminEditStoryController;
            var table = _$('#obj-cats');
            var input = _$('#ob-cat-input');
            var val = input.value;
            var tr = BlkLab.create('tr');
            var td = BlkLab.create('td');
            var td2 = BlkLab.create('td');
            td.innerHTML = val;
            td2.innerHTML = '<span class="fa fa-trash"></span>';
            tr.append([td,td2]);
            table.append(tr);
            self.refreshActions();
        }
    },

    inline: function(){
        var self = BlkLab.App.AdminEditStoryController;
        var cells = _$('.edit');
        cells.click(function(ev){
            if(ev.target.nodeName != 'INPUT'){
                var cell = _$(this);
                var idx = cell.data('idx');
                var input = BlkLab.create('input');
                var current = this.innerHTML;
                input.value = this.innerHTML;
                cell.innerHTML = '';
                cell.append(input);
                input.focus();
                input.onblur(function(){
                    if(current != input.value){
                        self.tips[idx] = input.value;
                    }
                    cell.innerHTML = input.value;
                    var model = Object.create(BlkLab.App.StoriesModel);
                    model.find({'_id': cell.data('id')}).then(function(m){
                        model.set('tips', self.tips);
                        model.save().then(function(http){
                            console.log(http.responseText);
                        });
                    });
                });
            }
        });
    },

    render: function(params){
        var self = this;
        var view = new BlkLab.App.AdminEditStoryView();
        BlkLab.App.StoriesModel.find({'_id': params.id}).then(function(model){
            self.tips = model.get('tips') || [];
            self.images = model.get('images') || [];
            self.objective_categories = model.get('objective_categories') || [];
            self.subjective_categories = model.get('subjective_categories') || [];
            if(self.tips.length == 0){
                self.images.forEach(function(){
                    self.tips.push('');
                });
            }
            BlkLab.App.AdminEditStoryController._id = model.get('_id');
            console.log(BlkLab.App.AdminEditStoryController._id)

            view.model = model;
            view.render('#content');
            self.inline();
            self.refreshActions();
        });
    }
});

BlkLab.App.AdminUsersController = BlkLab.Controller.extend({

    actions:{
        search: function(){
            var self = BlkLab.App.AdminUsersController;
            var ele = _$(this);
            var fields = ele.data('fields').split(',');

            var tags = _$('.book');
            var tag;
            var key;
            var val = ele.value.toLowerCase();

            self.data.forEach(function(row){
                var found = false;
                var tag = _$('#book-' + row._id);

                fields.forEach(function(key){
                    if(row[key]){
                        var value = row[key].toLowerCase();
                        if(found){
                            found = true;
                        }else if(!found && (val == "" || value.indexOf(val) != -1)){
                            found = true;
                        }else{
                            found = false;
                        }
                    }
                });

                if(found){
                    tag.css({
                        display:'table-row'
                    });
                }else{
                    tag.hide();
                }
            });
        }
    },
    data: [],

    inline: function(){
        var self = BlkLab.App.AdminController;
        var cells = _$('.inline');
        cells.click(function(ev){
            if(ev.target.nodeName != 'INPUT'){
                var cell = _$(this);
                var input = BlkLab.create('input');
                var current = this.innerHTML;
                input.value = this.innerHTML;
                cell.innerHTML = '';
                cell.append(input);
                input.focus();
                input.onblur(function(){
                    if(current != input.value){
                        var model = Object.create(BlkLab.App[cell.data('type') +'Model']);
                        model.find({'_id': cell.data('id')}).then(function(m){
                            model.set(cell.data('key'), input.value);
                            model.save().then(function(http){
                                console.log(http.responseText);
                            });
                        });
                    }
                    cell.innerHTML = input.value;
                });
            }
        });
    },

    render: function(params){
        var self = this;
        var view = new BlkLab.App.AdminUsersView();
        var model = Object.create(BlkLab.App.UsersModel);
        model.url = '/api/admin/users';
        model.find().then(function(model){
            self.data = model.data;
            view.model = model;
            view.render('#content');
            self.inline();
            self.refreshActions();

            _$('#search').on('change', self.actions.search);
        });
    }
});

BlkLab.App.AdminDashboardController = BlkLab.Controller.extend({

    actions:{},

    render: function(params){
        var self = this;
        var view = new BlkLab.App.AdminDashboardView();
        var model = Object.create(BlkLab.App.StatsModel);
        model.find().then(function(model){
            view.model = model;
            view.render('#content');
            self.refreshActions();
        });
    }
});

BlkLab.App.AdminFiltersController = BlkLab.Controller.extend({

    actions:{
        filterBy: function(){
            var self = BlkLab.App.AdminFiltersController;
            var view = new BlkLab.App.AdminFiltersView();
            var model = Object.create(BlkLab.App.FilterModel);
            var filter = this.value;
            if(!filter){
                filter = "profiles"
            }

            var titles = {
                "profiles": "All Users",
                "no-action": "Created profile but not taken any other action",
                "purchase-not-recorded": "Purchased a story but not recorded it",
                "recorded-not-shared": "Purchased and recorded a story but not shared it",
                "recorded-and-shared": "Purchased, recorded and shared a story",
                "invite-sent": "Invited someone to record a story",
                "invite-accepted": "Accepted an invitation to record a story",
                "invite-accepted-and-recorded": "Accepted invitation and recorded a story"
            }

            model.url = '/api/stats/filter/' + filter;
            model.find().then(function(model){
                view.model = model;
                view.render('#content');
                var header = _$('#filteredBy');
                header.innerHTML = titles[filter] + " (" + model.data.length + ")";
                _$('#filterBy').value = filter;
                self.refreshActions();
            });
        }
    },

    render: function(params){
        var self = this;
        self.actions.filterBy();
    }
});

BlkLab.App.Router.routes({
    '/admin': {
        controller: BlkLab.App.AdminController
    },

    '/admin/stories/:id': {
        controller: BlkLab.App.AdminEditStoryController
    },

    '/admin/users': {
        controller: BlkLab.App.AdminUsersController
    },

    '/admin/dashboard': {
        controller: BlkLab.App.AdminDashboardController
    },
    '/admin/dashboard/filter': {
        controller: BlkLab.App.AdminFiltersController
    },

    '/admin/dashboard/filter/:id': {
        controller: BlkLab.App.AdminFiltersController
    }
});
;(function() {
    var content = _$('#content');
    _$(window).on('load', function(){});
    _$(window).on('resize', function(){});

    /*_$(document).enter(function(ev){
        var form = _$('form');
        if(form){
            form.submit();
        }
    });*/

    function refresh(){};

    BlkLab.Security.handle = function(http){};


    BlkLab.History.start({
        callback: function(){
            refresh();
        }
    });
    BlkLab.App.run();
    refresh();

})();
