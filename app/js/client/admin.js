BlkLab.App.StoriesModel = BlkLab.Model.extend({});
BlkLab.App.StoriesModel.url = '/api/stories';

BlkLab.App.StatsModel = BlkLab.Model.extend({});
BlkLab.App.StatsModel.url = '/api/stats';

BlkLab.App.FilterModel = BlkLab.Model.extend({
    first_name: String,
    last_name: String,
    email: String
});
BlkLab.App.FilterModel.url = '/api/stats/filter';

BlkLab.App.AdminView = BlkLab.View.extend({
    template: this.views['./app/js/views/admin/admin.hbs']
});

BlkLab.App.AdminEditStoryView = BlkLab.View.extend({
    template: this.views['./app/js/views/admin/edit_story.hbs']
});

BlkLab.App.AdminUsersView = BlkLab.View.extend({
    template: this.views['./app/js/views/admin/users.hbs']
});

BlkLab.App.AdminDashboardView = BlkLab.View.extend({
    template: this.views['./app/js/views/admin/dashboard.hbs']
});

BlkLab.App.AdminFiltersView = BlkLab.View.extend({
    template: this.views['./app/js/views/admin/filters.hbs']
});

BlkLab.App.AdminController = BlkLab.Controller.extend({
    _id: '',

    actions:{
        createBook: function(ev){
            ev.preventDefault();
            var model = Object.create(BlkLab.App.StoriesModel);
            model.setData(BlkLab.Form.collect('form'));
            model.save().then(function(){
                location.reload(true);
            });
            return false;
        },

        loadFiles: function(ev){
            var cropmarks = _$('#cropmarks').value;
            for (var i = 0; i < this.files.length; i++) {
                BlkLab.Uploader.add_to_queue(this.files[i], {cropmarks: cropmarks, url: BlkLab.imageProcessor + '/process-assets/' + BlkLab.App.AdminController._id}, function(e){});
            }
            BlkLab.Uploader.onFinish = function(){
                location.reload(0);
            }
            BlkLab.Uploader.start();
        },

        search: function(){
            var self = BlkLab.App.AdminController;
            var ele = _$(this);
            var fields = ele.data('fields').split(',');

            var tags = _$('.book');
            var tag;
            var key;
            var val = ele.value.toLowerCase();

            self.data.forEach(function(row){
                var found = false;
                var tag = _$('#book-' + row._id);

                fields.forEach(function(key){
                    if(row[key]){
                        var value = row[key].toLowerCase();
                        if(found){
                            found = true;
                        }else if(!found && (val == "" || value.indexOf(val) != -1)){
                            found = true;
                        }else{
                            found = false;
                        }
                    }
                });

                if(found){
                    tag.css({
                        display:'table-row'
                    });
                }else{
                    tag.hide();
                }
            });
        },

        addBook: function(){
            if(_$('#book-detail').has_class('hide')){
                this.innerHTML = 'X';
            }else{
                this.innerHTML = '+';
            }

            _$('#book-detail').toggle_class('hide');
        }
    },
    data: [],

    inline: function(){
        var self = BlkLab.App.AdminController;
        var cells = _$('.inline');
        cells.click(function(ev){
            if(ev.target.nodeName != 'INPUT'){
                var cell = _$(this);
                var input = BlkLab.create('input');
                var current = this.innerHTML;
                input.value = this.innerHTML;
                cell.innerHTML = '';
                cell.append(input);
                input.focus();
                input.onblur(function(){
                    if(current != input.value){
                        var model = Object.create(BlkLab.App[cell.data('type') +'Model']);
                        model.find({'_id': cell.data('id')}).then(function(m){
                            model.set(cell.data('key'), input.value);
                            model.save().then(function(http){
                                console.log(http.responseText);
                            });
                        });
                    }
                    cell.innerHTML = input.value;
                });
            }
        });
    },

    render: function(params){
        var self = this;
        var view = new BlkLab.App.AdminView();
        var model = Object.create(BlkLab.App.StoriesModel);
        model.url = '/api/stories/all';
        model.find().then(function(model){
            self.data = model.data;
            view.model = model;
            view.render('#content');
            self.inline();
            self.refreshActions();

            _$('#search').on('change', self.actions.search);
        });
    }
});

BlkLab.App.AdminEditStoryController = BlkLab.Controller.extend({
    _id: '',
    tips: [],
    images: [],
    objective_categories: [],
    subjective_categories: [],

    actions:{
        save: function(ev){
            ev.preventDefault();
            var model = Object.create(BlkLab.App.StoriesModel);
            var formdata = BlkLab.Form.collect('form');
            model.setData(formdata);
            model.save({'_id': formdata._id}).then(function(){
                //location.reload(true);
            });
            return false;
        },

        optimize: function(){
            BlkLab.post({
                url: '/api/stories/optimize/' + BlkLab.App.AdminEditStoryController._id
            }).then(function(http){
                alert('done');
            });
        },

        genThumb: function(){
            BlkLab.post({
                url: '/api/stories/create/thumbnails/' + BlkLab.App.AdminEditStoryController._id
            }).then(function(http){
                alert('done');
            });
        },

        genFeatured: function(){
            BlkLab.post({
                url: '/api/stories/create/featured/' + BlkLab.App.AdminEditStoryController._id
            }).then(function(http){
                alert('done');
            });
        },

        loadFiles: function(ev){
            var cropmarks = _$('#cropmarks').value;
            for (var i = 0; i < this.files.length; i++) {
                BlkLab.Uploader.add_to_queue(this.files[i], {cropmarks: cropmarks, url: BlkLab.imageProcessor + '/process-assets/' + BlkLab.App.AdminEditStoryController._id}, function(e){});
            }
            BlkLab.Uploader.onFinish = function(){
                _$('#book-detail').show();
            }
            BlkLab.Uploader.start();
        },

        addObCat: function(){
            var self = BlkLab.App.AdminEditStoryController;
            var table = _$('#obj-cats');
            var input = _$('#ob-cat-input');
            var val = input.value;
            var tr = BlkLab.create('tr');
            var td = BlkLab.create('td');
            var td2 = BlkLab.create('td');
            td.innerHTML = val;
            td2.innerHTML = '<span class="fa fa-trash"></span>';
            tr.append([td,td2]);
            table.append(tr);
            self.refreshActions();
        }
    },

    inline: function(){
        var self = BlkLab.App.AdminEditStoryController;
        var cells = _$('.edit');
        cells.click(function(ev){
            if(ev.target.nodeName != 'INPUT'){
                var cell = _$(this);
                var idx = cell.data('idx');
                var input = BlkLab.create('input');
                var current = this.innerHTML;
                input.value = this.innerHTML;
                cell.innerHTML = '';
                cell.append(input);
                input.focus();
                input.onblur(function(){
                    if(current != input.value){
                        self.tips[idx] = input.value;
                    }
                    cell.innerHTML = input.value;
                    var model = Object.create(BlkLab.App.StoriesModel);
                    model.find({'_id': cell.data('id')}).then(function(m){
                        model.set('tips', self.tips);
                        model.save().then(function(http){
                            console.log(http.responseText);
                        });
                    });
                });
            }
        });
    },

    render: function(params){
        var self = this;
        var view = new BlkLab.App.AdminEditStoryView();
        BlkLab.App.StoriesModel.find({'_id': params.id}).then(function(model){
            self.tips = model.get('tips') || [];
            self.images = model.get('images') || [];
            self.objective_categories = model.get('objective_categories') || [];
            self.subjective_categories = model.get('subjective_categories') || [];
            if(self.tips.length == 0){
                self.images.forEach(function(){
                    self.tips.push('');
                });
            }
            BlkLab.App.AdminEditStoryController._id = model.get('_id');
            console.log(BlkLab.App.AdminEditStoryController._id)

            view.model = model;
            view.render('#content');
            self.inline();
            self.refreshActions();
        });
    }
});

BlkLab.App.AdminUsersController = BlkLab.Controller.extend({

    actions:{
        search: function(){
            var self = BlkLab.App.AdminUsersController;
            var ele = _$(this);
            var fields = ele.data('fields').split(',');

            var tags = _$('.book');
            var tag;
            var key;
            var val = ele.value.toLowerCase();

            self.data.forEach(function(row){
                var found = false;
                var tag = _$('#book-' + row._id);

                fields.forEach(function(key){
                    if(row[key]){
                        var value = row[key].toLowerCase();
                        if(found){
                            found = true;
                        }else if(!found && (val == "" || value.indexOf(val) != -1)){
                            found = true;
                        }else{
                            found = false;
                        }
                    }
                });

                if(found){
                    tag.css({
                        display:'table-row'
                    });
                }else{
                    tag.hide();
                }
            });
        }
    },
    data: [],

    inline: function(){
        var self = BlkLab.App.AdminController;
        var cells = _$('.inline');
        cells.click(function(ev){
            if(ev.target.nodeName != 'INPUT'){
                var cell = _$(this);
                var input = BlkLab.create('input');
                var current = this.innerHTML;
                input.value = this.innerHTML;
                cell.innerHTML = '';
                cell.append(input);
                input.focus();
                input.onblur(function(){
                    if(current != input.value){
                        var model = Object.create(BlkLab.App[cell.data('type') +'Model']);
                        model.find({'_id': cell.data('id')}).then(function(m){
                            model.set(cell.data('key'), input.value);
                            model.save().then(function(http){
                                console.log(http.responseText);
                            });
                        });
                    }
                    cell.innerHTML = input.value;
                });
            }
        });
    },

    render: function(params){
        var self = this;
        var view = new BlkLab.App.AdminUsersView();
        var model = Object.create(BlkLab.App.UsersModel);
        model.url = '/api/admin/users';
        model.find().then(function(model){
            self.data = model.data;
            view.model = model;
            view.render('#content');
            self.inline();
            self.refreshActions();

            _$('#search').on('change', self.actions.search);
        });
    }
});

BlkLab.App.AdminDashboardController = BlkLab.Controller.extend({

    actions:{},

    render: function(params){
        var self = this;
        var view = new BlkLab.App.AdminDashboardView();
        var model = Object.create(BlkLab.App.StatsModel);
        model.find().then(function(model){
            view.model = model;
            view.render('#content');
            self.refreshActions();
        });
    }
});

BlkLab.App.AdminFiltersController = BlkLab.Controller.extend({

    actions:{
        filterBy: function(){
            var self = BlkLab.App.AdminFiltersController;
            var view = new BlkLab.App.AdminFiltersView();
            var model = Object.create(BlkLab.App.FilterModel);
            var filter = this.value;
            if(!filter){
                filter = "profiles"
            }

            var titles = {
                "profiles": "All Users",
                "no-action": "Created profile but not taken any other action",
                "purchase-not-recorded": "Purchased a story but not recorded it",
                "recorded-not-shared": "Purchased and recorded a story but not shared it",
                "recorded-and-shared": "Purchased, recorded and shared a story",
                "invite-sent": "Invited someone to record a story",
                "invite-accepted": "Accepted an invitation to record a story",
                "invite-accepted-and-recorded": "Accepted invitation and recorded a story"
            }

            model.url = '/api/stats/filter/' + filter;
            model.find().then(function(model){
                view.model = model;
                view.render('#content');
                var header = _$('#filteredBy');
                header.innerHTML = titles[filter] + " (" + model.data.length + ")";
                _$('#filterBy').value = filter;
                self.refreshActions();
            });
        }
    },

    render: function(params){
        var self = this;
        self.actions.filterBy();
    }
});

BlkLab.App.Router.routes({
    '/admin': {
        controller: BlkLab.App.AdminController
    },

    '/admin/stories/:id': {
        controller: BlkLab.App.AdminEditStoryController
    },

    '/admin/users': {
        controller: BlkLab.App.AdminUsersController
    },

    '/admin/dashboard': {
        controller: BlkLab.App.AdminDashboardController
    },
    '/admin/dashboard/filter': {
        controller: BlkLab.App.AdminFiltersController
    },

    '/admin/dashboard/filter/:id': {
        controller: BlkLab.App.AdminFiltersController
    }
});
