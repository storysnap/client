BlkLab.App.UsersModel = BlkLab.Model.extend({});
BlkLab.App.UsersModel.url = '/api/users';

BlkLab.App.UsersResetView = BlkLab.View.extend({
    template: this.views['./app/js/views/users/recover.hbs'],
    title:'Kubby Professor Home'
});

BlkLab.App.UsersResetController = BlkLab.Controller.extend({
    token: '',

    actions:{
        updatePassword: function(){
            var self = BlkLab.App.UsersResetController;
            var password = _$('#password_field').value;
            var t = location.pathname.split('/').pop()
            BlkLab.post({
                url: '/api/users/auth/reset/' + self.token,
                data: JSON.stringify({
                    password: password
                }),
            }).then(function(http){
                var resp = JSON.parse(http.responseText);
                if(resp.token){
                    alert("Your password has been reset").
                    window.close();
                }else{
                    _$('#reset-form-message').innerHTML = 'Error Resetting Your Password<br><br>';
                }
            })
        }
    },

    render: function(params){
        this.token = params.id;
        var view = new BlkLab.App.UsersResetView();
        view.model = {
            data:{}
        };
        view.render('#content');
        this.refreshActions();
    }
});

BlkLab.App.Router.routes({
    '/users/reset/:id': {
        controller: BlkLab.App.UsersResetController
    }
});
